#include "plugin.hpp"
#include <cstring>
#include <random>
#include "samp.hpp"

// Add input 57 8B B9 ?? ?? ?? ?? 81 FF 90 00 00 00 7D
// Add entry 55 56 8B E9 57 8D BD ?? ?? ?? ?? 8D B5 ?? ?? ?? ?? B9

#pragma pack(push, 1)
class input {
public:
	char  pad[12];
	void *cmds[144];
	char  cmds_names[144][33];
	char  pad2[1568];
};
#pragma pack(pop)

plugin::plugin() {
	wndproc_hook_.install(std::make_tuple(this, &plugin::wndproc));
	proc_dmg_anim_hook_.install(std::make_tuple(this, &plugin::proc_dmg_anim));

	using namespace Xbyak::util;
	// Save EAX, return address, ECX and EDX
	cmd_mem_->mov(ptr[&cmd_context_.EAX], eax);
	cmd_mem_->pop(eax);
	cmd_mem_->mov(ptr[&cmd_context_.ret_addr], eax);
	cmd_mem_->mov(ptr[&cmd_context_.ECX], ecx);
	cmd_mem_->mov(ptr[&cmd_context_.EDX], edx);
	// Repush cmd params
	cmd_mem_->mov(edx, ptr[esp]);
	cmd_mem_->push(edx);
	// Call cmd handler
	cmd_mem_->mov(ecx, reinterpret_cast<std::uintptr_t>(this));
	auto cmd_handler_method = &plugin::cmd_handler;
	cmd_mem_->call(reinterpret_cast<void *&>(cmd_handler_method));
	// Restore EAX, return address, ECX and EDX
	cmd_mem_->mov(edx, ptr[&cmd_context_.EDX]);
	cmd_mem_->mov(ecx, ptr[&cmd_context_.ECX]);
	cmd_mem_->mov(eax, ptr[&cmd_context_.ret_addr]);
	cmd_mem_->push(eax);
	cmd_mem_->mov(eax, ptr[&cmd_context_.EAX]);
	// Return
	cmd_mem_->ret();

	using add_cmd_t	  = void(__thiscall *)(input *, const char *, void *);
	auto add_cmd_addr = samp::lib() + (samp::is_r1() ? 0x65AD0 : 0x69000);
	auto input_ptr	  = *reinterpret_cast<input **>(samp::lib() + (samp::is_r1() ? 0x21A0E8 : 0x26E8CC));
	reinterpret_cast<add_cmd_t>(add_cmd_addr)(input_ptr, "asc", cmd_mem_->getCode<void *>());
}

plugin::~plugin() {
	// Remove command
	auto input_ptr = *reinterpret_cast<input **>(samp::lib() + (samp::is_r1() ? 0x21A0E8 : 0x26E8CC));
	for (auto i = 0; i < 144; ++i) {
		if (input_ptr->cmds[i] == cmd_mem_->getCode<void *>()) {
			std::memset(&input_ptr->cmds[i], 0, 4);
			std::memset(input_ptr->cmds_names[i], 0, 33);
			std::memset(&input_ptr->cmds_names[i], 0, 4);
		}
	}
	delete cmd_mem_, cmd_mem_ = nullptr;
}

void plugin::cmd_handler(const char *params) {
	using add_entry_t = void(__thiscall *)(void *, int, const char *, const char *, std::uint32_t, std::uint32_t);

	auto chat_ptr		= *reinterpret_cast<void **>(samp::lib() + (samp::is_r1() ? 0x21A0E4 : 0x26E8C8));
	auto add_entry_addr = samp::lib() + (samp::is_r1() ? 0x64010 : 0x67460);

	int	 new_chance = -1;
	auto match		= sscanf_s(params, "%d", &new_chance);
	if (!match || match == EOF || new_chance < 0 || new_chance > 100) {
		reinterpret_cast<add_entry_t>(add_entry_addr)(chat_ptr, 8, "fuck off", nullptr, -1, 0);
		return;
	}
	chance_ = new_chance;
	reinterpret_cast<add_entry_t>(add_entry_addr)(chat_ptr, 8, "yes sir", nullptr, -1, 0);
}

LRESULT plugin::wndproc(wndproc_t orig, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	if (msg == WM_KEYUP) {
		switch (wparam) {
			case VK_F2:
				act_ = !act_;
				break;
			case VK_END:
				act_ = false;
				break;
			default:
				break;
		}
	}
	return orig(hwnd, msg, wparam, lparam);
}

void plugin::proc_dmg_anim(proc_dmg_anim_t orig, void *this_, void *ped, bool arg2) {
	if (ped != *reinterpret_cast<void **>(0xB6F5F0) || !act_) return orig(this_, ped, arg2);

	static std::random_device			   rd;
	static std::mt19937					   rgen(rd());
	static std::uniform_int_distribution<> rdis(0, 99);

	auto rval = rdis(rgen);
	if (rval >= chance_) return orig(this_, ped, arg2);
}
