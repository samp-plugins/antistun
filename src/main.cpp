#include <memory>
#include <lemon/hook.hpp>
#include "plugin.hpp"
#include "samp.hpp"

std::unique_ptr<plugin> plug;

void gameloop() {
	static bool init = false;
	if (init || !samp::inited()) return;
	plug = std::make_unique<plugin>();
	init = true;
}

BOOL APIENTRY DllMain(HMODULE, DWORD dwReasonForCall, LPVOID) {
	if (!samp::is_r1() && !samp::is_r3()) return FALSE;

	static lemon::hook<> gameloop_hook(0x748DA3);
	if (dwReasonForCall == DLL_PROCESS_ATTACH) {
		gameloop_hook.on_before += &gameloop;
		gameloop_hook.install();
	}
	return TRUE;
}
