#ifndef SAMP_HPP
#define SAMP_HPP

#include <cstdint>
#include <windows.h>

namespace samp {
	static std::uintptr_t lib() {
		static std::uintptr_t samp = 0;
		if (samp) return samp;

		samp = reinterpret_cast<std::uintptr_t>(GetModuleHandleA("samp.dll"));
		if (samp == -1) samp = 0;
		return samp;
	}

	static std::uintptr_t ep() {
		static std::uintptr_t ep = 0;
		if (ep) return ep;

		auto samp = lib();
		if (!samp) return 0;

		auto ntheader = reinterpret_cast<IMAGE_NT_HEADERS *>(samp + reinterpret_cast<IMAGE_DOS_HEADER *>(samp)->e_lfanew);
		ep			  = ntheader->OptionalHeader.AddressOfEntryPoint;
		return ep;
	}

	static bool is_r1() {
		return ep() == 0x31DF13;
	}

	static bool is_r3() {
		return ep() == 0xCC4D0;
	}

	static bool inited() {
		auto library = lib();
		if (!library) return false;

		return *reinterpret_cast<void **>(lib() + (is_r1() ? 0x21A0F8 : 0x26E8DC)) != nullptr;
	}
} // namespace samp

#endif // SAMP_HPP
