#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <random>
#include <lemon/detour.hpp>
#include <xbyak/xbyak.h>

using wndproc_t		  = LRESULT(CALLBACK *)(HWND, UINT, WPARAM, LPARAM);
using proc_dmg_anim_t = void(__thiscall *)(void *, void *, bool);

using cmd_context = struct {
	std::uint32_t EAX;
	std::uint32_t ECX;
	std::uint32_t EDX;

	std::uint32_t ret_addr;
};

class plugin {
	lemon::detour<wndproc_t>	   wndproc_hook_{ 0x747EB0 };
	lemon::detour<proc_dmg_anim_t> proc_dmg_anim_hook_{ 0x4B3FC0 };

	/// cfg
	bool act_	 = false;
	int	 chance_ = 50;

public:
	plugin();
	~plugin();

private:
	Xbyak::CodeGenerator *cmd_mem_ = new Xbyak::CodeGenerator();
	cmd_context			  cmd_context_{};

	void cmd_handler(const char *param);

	LRESULT wndproc(wndproc_t orig, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	void	proc_dmg_anim(proc_dmg_anim_t orig, void *this_, void *ped, bool arg2);
};

#endif // PLUGIN_HPP
